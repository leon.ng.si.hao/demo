resource "aws_vpc" "demo" {
    cidr_block = "172.32.0.0/16"
    enable_dns_support = "true"
    enable_dns_hostnames = "true"
    enable_classiclink = "false"
    instance_tenancy = "default"

    tags = {
        Name = "demo"
    }
}

resource "aws_subnet" "public-subnet" {
    vpc_id = "${aws_vpc.demo.id}"
    cidr_block = "172.32.1.0/24"
    map_public_ip_on_launch = "true"
    availability_zone = "us-east-1a"
    tags = {
        Name = "public-subnet"
    }
}

resource "aws_subnet" "private-subnet" {
    vpc_id = "${aws_vpc.demo.id}"
    cidr_block = "172.32.0.0/24"
    availability_zone = "us-east-1a"
    tags = {
        Name = "private-subnet"
    }
}
