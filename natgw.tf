#Create EIP
resource "aws_eip" "demo-nat-eip" {
vpc = true
tags = {
    Name = "demo-nat-eip"
}
}

resource "aws_nat_gateway" "demo-nat-gw" {
  allocation_id = aws_eip.demo-nat-eip.id
  subnet_id = aws_subnet.public-subnet.id
tags = {
    Name = "demo-nat-gw"
}
}

resource "aws_route_table" "Private-RTB" {
  vpc_id = "${aws_vpc.demo.id}"
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.demo-nat-gw.id
  }

  tags = {
    Name = "Private-RTB"
  }
}

resource "aws_route_table_association" "Private-RTB" {
  subnet_id = aws_subnet.private-subnet.id
  route_table_id = aws_route_table.Private-RTB.id
}
