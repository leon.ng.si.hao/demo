resource "aws_lb" "main" {
  load_balancer_type = "network"
  name  = "demo-nlb"

  subnet_mapping {
    subnet_id = "${aws_subnet.public-subnet.id}"
  }

  tags = {
    Name  = "demo-nlb"
  }
}

resource "aws_lb_listener" "main" {
  load_balancer_arn = aws_lb.main.id
  port = 80
  protocol = "TCP"

  default_action {
    target_group_arn = aws_lb_target_group.main.id
    type  = "forward"
  }
}

resource "aws_lb_listener" "ssh" {
  load_balancer_arn = aws_lb.main.id
  port = 22
  protocol = "TCP"

  default_action {
    target_group_arn = aws_lb_target_group.ssh.id
    type  = "forward"
  }
}

resource "aws_lb_target_group" "main" {

  protocol = "TCP"
  vpc_id = "${aws_vpc.demo.id}"
  target_type = "instance"
  port = 80
  name = "nlb-tg-80"
  tags = {
    Name  = "nlb-tg-80"
  }
}

resource "aws_lb_target_group" "ssh" {

  protocol = "TCP"
  vpc_id = "${aws_vpc.demo.id}"
  target_type = "instance"
  port = 22
  name = "nlb-tg-22"
  tags = {
    Name  = "nlb-tg-22"
  }
}

resource "aws_lb_target_group_attachment" "main" {
  target_group_arn = aws_lb_target_group.main.arn
  target_id = aws_instance.private-app.id
  port = 80
}

resource "aws_lb_target_group_attachment" "ssh" {
  target_group_arn = aws_lb_target_group.ssh.arn
  target_id = aws_instance.private-app.id
  port = 22
}
