resource "aws_security_group" "Default-SG" {
    vpc_id = "${aws_vpc.demo.id}"

    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"

        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["118.189.0.0/16", "116.206.0.0/16", "223.25.0.0/16"]
    }
    tags = {
        Name = "Default-SG"
    }
}
