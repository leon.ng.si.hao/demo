#Creating Bastion Host
resource "aws_instance" "bastion-host" {
    instance_type = "t2.micro"
    ami = "ami-09e67e426f25ce0d7"
    # VPC
    subnet_id = "${aws_subnet.public-subnet.id}"
    # Security Group
    vpc_security_group_ids = ["${aws_security_group.Default-SG.id}"]
    key_name = "demo-kp"
    tags = {
        Name = "bastion-host"
    }
}



#Creating Private Web
resource "aws_instance" "private-app" {
    instance_type = "t2.micro"
    ami = "ami-09e67e426f25ce0d7"
    # VPC
    subnet_id = "${aws_subnet.private-subnet.id}"
    # Security Group
    vpc_security_group_ids = ["${aws_security_group.Default-SG.id}"]
    # the Public SSH key
    key_name = "demo-kp"

    #user data
    user_data = <<-EOF
                    #!/bin/bash
                    sudo su
                    apt-get update -y && apt-get install -y docker.io
                    docker pull nginx
                    echo -e '<html> \n<head> </head> \n<body> \n<h1>Hello Merquri Team!</h1> \n<h2>Ng Si Hao, Leon. https://www.linkedin.com/in/ngsihaoleon</h2> \n</body> \n</html>' > /home/ubuntu/index.html
                    docker run -it --rm -d -p 80:80 --name web -v /home/ubuntu:/usr/share/nginx/html nginx
            EOF
    tags = {
        Name = "private-web"
    }
}
