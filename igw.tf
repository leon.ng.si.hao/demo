#Create Internet Gateway
resource "aws_internet_gateway" "demo-igw" {
    vpc_id = "${aws_vpc.demo.id}"
    tags = {
        Name = "demo-igw"
    }
}

#Create Public Route Table
resource "aws_route_table" "Public-RTB" {
  vpc_id = "${aws_vpc.demo.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.demo-igw.id
  }
  tags = {
    Name = "Public-RTB"
  }
}

#Associate Public Subnet to Public Route Table
resource "aws_route_table_association" "Public-RTB" {
  subnet_id      = aws_subnet.public-subnet.id
  route_table_id = aws_route_table.Public-RTB.id
}
